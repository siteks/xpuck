
// Top level of Shim FPGA
// 
// The FPGA is an iCE40HX1KTQFP144
//
// RAM4K    16
// PLL      1
// IO       95
//
//

module shim(
    input   usb_clk,

    input   xu4_spi1_clk,
    input   xu4_spi1_csn,
    input   xu4_spi1_mosi,

    output  leds_din,

    output  epuck_cam_clk,

    // USB FIFO interface
    input   fifo_clk,
    inout   adbus0,
    inout   adbus1,
    inout   adbus2,
    inout   adbus3,
    inout   adbus4,
    inout   adbus5,
    inout   adbus6,
    inout   adbus7,
    input   rxfn,
    input   txen,
    output  rdn,
    output  reg wrn,
    output  oen,
    output  siwu,

    // Camera interface
    input   epuck_pclk,
    input   epuck_vsync,
    input   epuck_href,
    input   epuck_y0,
    input   epuck_y1,
    input   epuck_y2,
    input   epuck_y3,
    input   epuck_y4,
    input   epuck_y5,
    input   epuck_y6,
    input   epuck_y7,

    // debug
    output  fpgaspare7
);


// wire    clk;
// 
// SB_PLL40_CORE   pll1 (
//     .REFERENCECLK   (usb_clk),
//     .RESETB         (1'b1),
//     .BYPASS         (1'b0),
//     .PLLOUTGLOBAL   (clk)
// );
// 
// // freq = fin * (DIVF+1) / (2^DIVQ * (DIVR+1))
// // DIVR = 0:15
// // DIVF = 0:63
// // DIVQ = 1:6
// // Fvco = fin * (DIVF+1) / (DIVR+1)
// // min=533, max=1066
// // Fpfd = fin / (DIVR+1)
// // min=10, max=133
// defparam    pll1.FEEDBACK_PATH                      = "SIMPLE";
// defparam    pll1.DELAY_ADJUSTMENT_MODE_FEEDBACK     = "FIXED";
// defparam    pll1.FDA_FEEDBACK                       = 4'b0000;
// defparam    pll1.DELAY_ADJUSTMENT_MODE_RELATIVE     = "FIXED";
// defparam    pll1.FDA_RELATIVE                       = 4'b0000;
// defparam    pll1.SHIFTREG_DIV_MODE                  = 2'b00;
// defparam    pll1.PLLOUT_SELECT                      = "GENCLK";
// // Filter must not be zero, acts as bypass if it is
// defparam    pll1.FILTER_RANGE                       = 3'b001;
// defparam    pll1.ENABLE_ICEGATE                     = 1'b0;
// defparam    pll1.DIVR   = 4'b0000;
// defparam    pll1.DIVF   = 7'b0111111;
// defparam    pll1.DIVQ   = 3'b011;

//---------------------------------------------------------------------
// Camera 
//---------------------------------------------------------------------
wire            cam_data_valid;
wire    [7:0]   cam_data;
wire    [7:0]   fifo_data;
wire            fifo_valid;
wire            fifo_enable;

// Give the camera a clock. Poss change this to PLL if we want faster
// framerate
assign epuck_cam_clk = usb_clk;

// Turn the camera data into a bus
assign cam_data = { epuck_y7, 
                    epuck_y6, 
                    epuck_y5, 
                    epuck_y4, 
                    epuck_y3, 
                    epuck_y2, 
                    epuck_y1, 
                    epuck_y0};

// There is valid camera data when both sync lines are high
assign cam_data_valid = epuck_vsync & epuck_href;

// Stuff synchronisation bytes into stream. 0x00 and 0xff never occur
// in the camera data stream so use 0x00 to indicate vsync and 0xff to
// indicate hsync.
// At the start of a frame the sequence 0xff 0x00 will occurr
// At the start of a line the value 0xff will occur
reg last_vsync, last_hsync;
reg [6:0] count;
always @(posedge epuck_pclk)
begin
    last_vsync <= epuck_vsync;
    last_hsync <= epuck_href;
    count <= cam_data_valid ? count + 1 : 0;
end
wire eof = last_vsync & ~epuck_vsync;
wire eol = last_hsync & ~epuck_href;
wire cdv = cam_data_valid | eof | eol;
//wire [7:0]cd = ~cdv4 & cam_data_valid ? (~vs4 ? 8'hfe : 8'hff) : cam_data;
wire [7:0]cd = eof ? 8'h00 : eol ? 8'hff : cam_data;
//wire [7:0]cd = eof ? 8'h00 : eol ? 8'hff : ({1'b0,count} + 8'h10);

fifo4096x8 U_fifo (
    .din_clk    (epuck_pclk),
    //.din_valid  (cam_data_valid),
    .din_valid  (cdv),
    // This should never go low, we can't stall incoming data
    .din_enable (fpgaspare7),
    //.din        (cam_data),
    .din        (cd),

    .dout_clk   (fifo_clk),
    .dout_valid (fifo_valid),
    .dout_enable(fifo_enable),
    .dout       (fifo_data)
);

// Interface to FT2232H
reg [7:0]   adata;
wire        dout_en;

// We don't care about power up at this point
assign siwu     = 1'b1;
// Don't output from ft2232, this may need handling properly
// to drain fifo if host writes to ft2232h, even though we just throw 
// the data away
assign oen      = 1'b1;
// Don't read from fifo
assign rdn      = 1'b1;

// Might want to do bidirectional one day..
assign dout_en  = 1'b1;
assign adbus0 = dout_en ? adata[0] : 1'bz;
assign adbus1 = dout_en ? adata[1] : 1'bz;
assign adbus2 = dout_en ? adata[2] : 1'bz;
assign adbus3 = dout_en ? adata[3] : 1'bz;
assign adbus4 = dout_en ? adata[4] : 1'bz;
assign adbus5 = dout_en ? adata[5] : 1'bz;
assign adbus6 = dout_en ? adata[6] : 1'bz;
assign adbus7 = dout_en ? adata[7] : 1'bz;




always @(posedge fifo_clk)
begin
    // Register the outputs
    wrn     <= ~(~txen & fifo_valid);
    adata   <= fifo_data;
end

// Transfer occurs in this condition so pull from fifo
assign fifo_enable = ~txen;



//---------------------------------------------------------------------
// LED control
//---------------------------------------------------------------------


spi_listener U_spi_listener (
    .clk        (usb_clk),
    .spi_clk    (xu4_spi1_clk),
    .spi_csn    (xu4_spi1_csn),
    .spi_mosi   (xu4_spi1_mosi),
    .led_data   (leds_din)
);







endmodule

