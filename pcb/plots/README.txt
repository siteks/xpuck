README file for PCB 'shim1_pcb'
===============================

Board specification:
====================

Size            70mm x 70mm
Material        FR4
Layers          4
Thickness       1.6
Finish          Electroless Nickel/Immersion Gold
Solder mask     Green
CU weight       1.0oz


Min track thickness     0.15 mm
Min track gap           0.15 mm
Min hole                0.3 mm
Min via                 0.6 mm
Min inner track         0.2 mm
Min inner gap           0.3 mm
Soldermask clearance    0.05 mm
Soldermask min width    0.08 mm

Layer stack:
============

Top silk screen         shim1_pcb-F.SilkS.gto
Top solder mask         shim1_pcb-F.Mask.gts
Top copper              shim1_pcb-F.Cu.gtl
Inner copper 1          shim1_pcb-In1.Cu.g2
Inner copper 2          shim1_pcb-In2.Cu.g3
Bottom copper           shim1_pcb-B.Cu.gbl
Bottom solder mask      shim1_pcb-B.Mask.gbs
Board outline           shim1_pcb-Edge.Cuts.gm1

Drill                   shim1_pcb.drl
NPTH drill              shim1_pcb-NPTH.drl

Top paste stencil       shim1_pcb-F.Paste.gtp


